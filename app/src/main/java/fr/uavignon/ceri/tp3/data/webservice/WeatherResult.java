package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {

    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo) {

        cityInfo.setTempKelvin(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindSpeedMPerS(weatherInfo.wind.speed);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setLastUpdate(weatherInfo.dt);

        if(weatherInfo.weather!=null){
            cityInfo.setIcon(weatherInfo.weather.get(0).icon);
            cityInfo.setDescription(weatherInfo.weather.get(0).description);
        }
    }
}
